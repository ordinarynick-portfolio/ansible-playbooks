# Ansible Playbooks for Server Setup

Simple yet secure Ansible Playbooks for initial server setup and hardening.

## Security Features

- Automatic system updates with unattended-upgrades
- UFW firewall configuration with secure defaults
- Fail2ban for intrusion prevention
- Secure SSH configuration
- System hardening practices

## Prerequisites

1. Ansible 2.9 or higher on control machine
2. Target server with SSH access
3. Python 3.x on target server

## Quick Start

1. Set environment variables for secure access:
```bash
# Set these before running ansible
export ANSIBLE_USER=your_username     # SSH user for connection
export ANSIBLE_SUDO_PASS=your_pass    # Sudo password if required
export ANSIBLE_SSH_PASS=your_pass     # SSH password (only for first connection)
```

Note: The SSH password is only needed for the first connection. After the initial setup, it's recommended to:
1. Set up SSH keys for authentication
2. Disable password authentication
3. Remove the ANSIBLE_SSH_PASS environment variable

2. Configure inventory:
```ini
[debian_server]
your_server_ip
```

3. Run playbook:
```bash
ansible-playbook setup_debian.yaml
```

## Security Measures

### Installed Security Packages
- ufw (Firewall)
- fail2ban (Intrusion prevention)
- unattended-upgrades (Automatic updates)

### Basic Utilities
- mc (Midnight Commander)
- jq (JSON processor)
- vim (Text editor)
- curl, gnupg, etc.

### Security Best Practices
1. Use non-root user with sudo privileges
2. Enable and configure UFW firewall
3. Keep system updated automatically
4. Monitor logs with fail2ban
5. Use SSH key authentication when possible
6. Keep sensitive data in environment variables

## Repository Structure

```
.
├── ansible.cfg           # Secure configuration defaults
├── inventory.ini         # Server inventory
├── setup_debian.yaml     # Main Debian setup playbook
├── group_vars/
│   └── debian_server/   # Server group configurations
└── roles/
    └── debian/          # Debian setup role
```

## Alternative Usage Methods

1. Command line arguments:
```bash
ansible-playbook setup_debian.yaml -e "ansible_user=your_user ansible_become_pass=your_pass"
```

2. Original format (not recommended):
```bash
ansible-playbook setup_debian.yaml -l server-ip -u user -i server-ip,
```

Using environment variables is recommended for better security and maintainability.

## CI/CD Pipeline

This repository includes a GitLab CI/CD pipeline that ensures code quality and security. The pipeline consists of the following stages:

### Stages and Jobs

1. **Lint Stage**
   - `yaml-lint`: Validates all YAML files for syntax and style
   - Reports are saved as artifacts in `yamllint-report.txt`

2. **Ansible Check Stage**
   - `ansible-lint`: Checks Ansible best practices and common mistakes
   - `ansible-playbook-check`: Validates playbook syntax
   - Lint reports are saved as artifacts in `ansible-lint-report.txt`

3. **Security Stage**
   - `security-check`: Scans Python dependencies for known vulnerabilities
   - Results are saved in `security-report.txt`

### Configuration Files

- `.gitlab-ci.yml`: Main pipeline configuration
- `.yamllint`: YAML linting rules
- `.ansible-lint`: Ansible linting configuration

### Pipeline Features

- Automatic retries for flaky jobs
- Artifact preservation for all reports
- Proper job dependencies
- Security scanning (non-blocking)
- Pipeline badges for status visibility

### Running Locally

To run checks locally before committing:

```bash
# Install required tools
pip install yamllint ansible-lint safety

# Run checks
yamllint .
ansible-lint
ansible-playbook --syntax-check setup_debian.yaml
safety check
```

## Security Notes

1. Never commit sensitive data to the repository
2. Use SSH keys instead of passwords when possible
3. Keep sudo passwords secure and change them regularly
4. Review UFW rules after installation
5. Check fail2ban logs periodically
6. Review CI/CD reports regularly for security issues
